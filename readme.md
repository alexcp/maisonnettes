# Maisonnettes sur le cap

## Instructions
Installer [Ruby](https://www.ruby-lang.org/).
Dans une ligne de commande installer Bundler avec `gem install bundler`.
Dans le dossier du site executer `bundle install`.
Maintenant vous pouvez compiler le site avec `middleman build`